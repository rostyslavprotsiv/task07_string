package com.rostyslavprotsiv.controller;

import com.rostyslavprotsiv.model.action.LocaleManager;
import com.rostyslavprotsiv.model.action.RegExAction;
import com.rostyslavprotsiv.model.action.StringUtils;
import com.rostyslavprotsiv.model.creator.TestObjectCreator;
import com.rostyslavprotsiv.model.entity.TestObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;

public final class Controller {
    private final Logger LOGGER = LogManager.getLogger(Controller.class);
    private final StringUtils STRING_UTILS = new StringUtils();
    private final TestObject[] TEST_OBJECT_MASS =
            new TestObjectCreator().creator();
    private final LocaleManager LOCALE_MANAGER = new LocaleManager();
    private final RegExAction REG_EX_ACTION = new RegExAction();

    @SafeVarargs
    public final <T> String concatenateAll(final T... allObjects) {
        return STRING_UTILS.concatenate(allObjects);
    }

    public TestObject[] getTestObjects() {
        return TEST_OBJECT_MASS;
    }

    public ArrayList<String> getEnglishMenu() {
        return LOCALE_MANAGER.getEnglishMenu();
    }

    public ArrayList<String> getUkrainianMenu() {
        return LOCALE_MANAGER.getUkrainianMenu();
    }

    public ArrayList<String> getSpainMenu() {
        return LOCALE_MANAGER.getSpainMenu();
    }

    public boolean isBeginCapitalEndPeriod(final String sentence) {
        return REG_EX_ACTION.beginCapitalEndPeriod(sentence);
    }

    public String[] getSplitOnTheGivenWords(final String sentence,
                                            final String... words) {
        String[] result = REG_EX_ACTION.splitOnTheGivenWords(sentence, words);
        if (result == null) {
            String logMessage = "Error with splitting";
            LOGGER.info(logMessage);
            LOGGER.warn(logMessage);
            LOGGER.error(logMessage);
            throw new IllegalArgumentException();
        }
        return result;
    }

    public String getReplacedVowelsWithUnderscores(final String sentence) {
        return REG_EX_ACTION.replaceVowelsWithUnderscores(sentence);
    }
}
