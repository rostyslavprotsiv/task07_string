package com.rostyslavprotsiv.controller;

import com.rostyslavprotsiv.model.action.textaction.TextAction;
import com.rostyslavprotsiv.model.entity.Text;

import java.util.List;
import java.util.Set;

public class TextController {
    private final Text TEXT = new Text("Text.txt");
    private final TextAction TEXT_ACTION = new TextAction();

    public int getMoreSimilarSentences() {
        return TEXT_ACTION.findMoreSimilarSentences(TEXT);
    }

    public List<String> getByIncreasingWords() {
        return TEXT_ACTION.getAllByIncreasingWords(TEXT);
    }

    public String getExceptWord() {
        return TEXT_ACTION.findSpecificWord(TEXT);
    }

    public Set<String> getQuestionsWords(int length) {
        return TEXT_ACTION.getWordsFromQuestions(TEXT, length);
    }

    public List<String> replaceFirstVowelsWithLargest() {
        return TEXT_ACTION.replaceStartedVowelsWithLargest(TEXT);
    }

    public List<String> sortWithFirstLetter() {
        return TEXT_ACTION.getSortedByFirstLetter(TEXT);
    }

    public List<String> getByPercentsOfVowels() {
        return TEXT_ACTION.getByPercentsOfVowels(TEXT);
    }

    public List<String> sortStartingVowelsWithConsonant() {
        return TEXT_ACTION.sortStartedVowelsWithConsonant(TEXT);
    }

    public List<String> sortIncreasingByLetter(final char letter) {
        return TEXT_ACTION.sortIncreasingByGivenLetter(TEXT, letter);
    }

    public List<String> outFrequencyOfGivenWords(final List<String> words) {
        return TEXT_ACTION.getFrequencyGivenWords(TEXT, words);
    }

    public List<String> deleteMaxSubstring(final char start, final char end) {
        return TEXT_ACTION.deleteSubstring(TEXT, start, end);
    }

    public List<String> deleteWithFirstConsonant(final int length) {
        return TEXT_ACTION.deleteStartingOnConsonant(TEXT, length);
    }

    public List<String> sortDescendingByLetter(final char letter) {
        return TEXT_ACTION.sortDescendingByGivenLetter(TEXT, letter);
    }

    public String getPalindrome() {
        return TEXT_ACTION.findPalindrome(TEXT);
    }

    public String deleteEntriesOfFirstLetter() {
        return TEXT_ACTION.deleteEntriesFirstLetter(TEXT);
    }

    public List<String> replaceInSentence(final int sentenceNumber,
                                          final int lengthOfReplaceableWords,
                                          final String subString) {
        return TEXT_ACTION.replaceGivenSentence(TEXT, sentenceNumber,
                lengthOfReplaceableWords, subString);
    }
}
