package com.rostyslavprotsiv.view;

import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RegExMenu extends AbstractMenu {
    private String sentence;
    private final String[] wordsForSplitting = new String[]{"the", "you"};

    public RegExMenu() {
        menu.put(0, "Begins with a capital and ends with a period");
        menu.put(1,
                "Get split parts on the given words " + getGivenWordsForMenu());
        menu.put(2, "Get sentence with replaced vowels with underscores");
        menu.put(3, "Input again");
        menu.put(4, "Exit");
        methodsForMenu.put(0, this::printBeginsCapitalEndsPeriod);
        methodsForMenu.put(1, this::printSplitWords);
        methodsForMenu.put(2, this::printWithReplacedVowels);
        methodsForMenu.put(3, this::input);
        methodsForMenu.put(4, this::quit);
    }

    public void show() {
        input();
        out();
    }

    @Override
    protected void showInfo() {
        LOGGER.info("Menu for RegEx");
        menu.forEach((key, elem) -> LOGGER.info(key + " : " + elem));
    }

    private void input() {
        StringBuilder stringBuilder = new StringBuilder();
        LOGGER.info("Please, input needed text");
        String inputted = scan.nextLine();
        do {
            stringBuilder.append(inputted);
            inputted = scan.nextLine();
        } while (!inputted.equals(""));
        sentence = stringBuilder.toString();
    }

    private void printBeginsCapitalEndsPeriod() {
        LOGGER.info(CONTROLLER.isBeginCapitalEndPeriod(sentence));
    }

    private void printSplitWords() {
        Stream.of(CONTROLLER.getSplitOnTheGivenWords(sentence,
                wordsForSplitting))
                .forEach(LOGGER::info);
    }

    private void printWithReplacedVowels() {
        LOGGER.info(CONTROLLER.getReplacedVowelsWithUnderscores(sentence));
    }

    private String getGivenWordsForMenu() {
        String words = "";
        words += "(";
        words += Stream.of(wordsForSplitting)
                .map(s -> "\"" + s + "\", ")
                .collect(Collectors.joining());
        words = words.substring(0, words.lastIndexOf(","));
        words += ")";
        return words;
    }
}
