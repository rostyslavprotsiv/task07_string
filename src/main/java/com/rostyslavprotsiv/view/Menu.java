package com.rostyslavprotsiv.view;

import java.util.Arrays;

public final class Menu extends AbstractMenu {
    private final LocalizedMenu LOCALIZED_MENU = new LocalizedMenu();
    private final RegExMenu REG_EX_MENU = new RegExMenu();
    private final TextMenu TEXT_MENU = new TextMenu();

    public Menu() {
        menu.put(0, "First task(Object concatenation)");
        menu.put(1, "Second task(Localized menu)");
        menu.put(2, "Third task(Work with RegEx)");
        menu.put(3, "Fourth task(Work with String and RegEx)");
        menu.put(4, "Exit");
        methodsForMenu.put(0, this::showFirstTask);
        methodsForMenu.put(1, this::showSecondTask);
        methodsForMenu.put(2, this::showThirdTask);
        methodsForMenu.put(3, this::showFourthTask);
        methodsForMenu.put(4, this::quit);
    }

    public void show() {
        out();
    }

    @Override
    protected void showInfo() {
        LOGGER.info("Menu for task07_String");
        menu.forEach((key, elem) -> LOGGER.info(key + " : " + elem));
    }

    private void showFirstTask() {
        LOGGER.info(Arrays.toString(CONTROLLER.getTestObjects()));
        LOGGER.info(CONTROLLER.concatenateAll(CONTROLLER.getTestObjects()));
    }

    private void showSecondTask() {
        LOCALIZED_MENU.show();
    }

    private void showThirdTask() {
        REG_EX_MENU.show();
    }

    private void showFourthTask() {
        TEXT_MENU.show();
    }
}
