package com.rostyslavprotsiv.view;

import com.rostyslavprotsiv.controller.Controller;
import com.rostyslavprotsiv.view.interfaces.Printable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;


public class LocalizedMenu {
    private final Controller CONTROLLER = new Controller();
    private Scanner scan = new Scanner(System.in, "UTF-8");
    private final Logger LOGGER = LogManager.getLogger(LocalizedMenu.class);
    private Map<Integer, Printable> methodsForMenu = new HashMap<>();
    private ArrayList<String> currentMenu = CONTROLLER.getEnglishMenu();
    private boolean errorAppeared;

    private enum Color {
        ANSI_RED("\u001B[31m"), ANSI_WHITE("\u001B[37m"),
        ANSI_BLUE("\u001B[34m"), ANSI_RESET("\u001B[0m");
        private String ansiCode;

        Color(String ansiCode) {
            this.ansiCode = ansiCode;
        }

    }

    public LocalizedMenu() {
        methodsForMenu.put(0, this::printEnglishMenu);
        methodsForMenu.put(1, this::printUkrainianMenu);
        methodsForMenu.put(2, this::printSpainMenu);
        methodsForMenu.put(3, this::exit);
    }

    public void show() {
        int inputted = 0;
        int previousInputted = 0;
        Optional<Printable> opt;
        outMenu(currentMenu);
        do {
            if (!errorAppeared) {
                previousInputted = inputted;
            }
            scan = new Scanner(System.in, "UTF-8");
            if (scan.hasNextInt()) {
                inputted = scan.nextInt();
                if (previousInputted == inputted) {
                    LOGGER.debug("Only reading from existing ArrayList");
                    outMenu(currentMenu);
                    errorAppeared = false;
                    continue;
                }
                errorAppeared = false;
                opt = Optional.ofNullable(methodsForMenu.get(inputted));
                opt.orElse(this::badInput)
                        .print();
            } else {
                badInput();
            }
        } while (inputted != methodsForMenu.size() - 1);
    }

    private void printEnglishMenu() {
        outMenu(currentMenu = CONTROLLER.getEnglishMenu());
    }

    private void printUkrainianMenu() {
        outMenu(currentMenu = CONTROLLER.getUkrainianMenu());
    }

    private void printSpainMenu() {
        outMenu(currentMenu = CONTROLLER.getSpainMenu());
    }

    private void outMenu(final ArrayList<String> menu) {
        StringBuilder builder = new StringBuilder();
        builder.append("\n Localized menu \n");
        for (int i = 0; i < menu.size() - 2; i++) {
            builder.append("Enter " + i + " for : " + menu.get(i) + "\n");
        }
        builder.append(menu.get(menu.size() - 2));
        transformColor(menu, builder);
        LOGGER.info(builder);
    }

    private void transformColor(final ArrayList<String> menu,
                                final StringBuilder builder) {
        if (menu.get(menu.size() - 1)
                .equals("red")) {
            builder.insert(0, Color.ANSI_RED.ansiCode);
        }
        if (menu.get(menu.size() - 1)
                .equals("white")) {
            builder.insert(0, Color.ANSI_WHITE.ansiCode);
        }
        if (menu.get(menu.size() - 1)
                .equals("blue")) {
            builder.insert(0, Color.ANSI_BLUE.ansiCode);
        }
        builder.append(Color.ANSI_RESET.ansiCode);
    }

    private void exit() {
        LOGGER.info("Bye Bye!");
    }

    private void badInput() {
        LOGGER.error("You entered bad value");
        errorAppeared = true;
    }
}
