package com.rostyslavprotsiv.view;

import com.rostyslavprotsiv.controller.TextController;

import java.util.ArrayList;
import java.util.List;

public class TextMenu extends AbstractMenu {
    private final TextController CONTROLLER = new TextController();

    public TextMenu() {
        menu.put(0, "Find more count of sentences with similar words.");
        menu.put(1, "Out all sentences by increasing all words.");
        menu.put(2, "Find word in first sentence that is absent in other.");
        menu.put(3, "Out words given length in questions.");
        menu.put(4, "Replace first word that starts from vowel with the " +
                "largest word in all text.");
        menu.put(5, "Print all words sorted by alphabet.");
        menu.put(6, "Sort all words by increasing percents of vowels.");
        menu.put(7, "Words, that start with vowel, sort by first consonant.");
        menu.put(8, "All words sort by increasing number of the given letter " +
                "in the word.");
        menu.put(9, "For each word from given list sort them in descending " +
                "order of frequency of repetition.");
        menu.put(10, "Delete substring of max length in each sentence which " +
                "starts and ends with given letters.");
        menu.put(11, "Delete all words of given length that start on " +
                "consonant.");
        menu.put(12, "All words sort by decreasing number of the given letter" +
                " in the word.");
        menu.put(13, "Find palindrome of maximum length.");
        menu.put(14, "Delete all entries of first letter in each word in the " +
                "text.");
        menu.put(15, "All words of given length replace given substring in " +
                "given sentence.");
        menu.put(16, "Exit");
        methodsForMenu.put(0, this::outMoreSimilarSentences);
        methodsForMenu.put(1, this::outByIncreasingWords);
        methodsForMenu.put(2, this::outExceptWord);
        methodsForMenu.put(3, this::outQuestionsWords);
        methodsForMenu.put(4, this::outReplacedFirstVowelsWithLargest);
        methodsForMenu.put(5, this::outSortedWithFirstLetter);
        methodsForMenu.put(6, this::outByPercentsOfVowels);
        methodsForMenu.put(7, this::outSortedStartingVowelsWithConsonant);
        methodsForMenu.put(8, this::outSortedIncreasingByLetter);
        methodsForMenu.put(9, this::printFrequencyOfGivenWords);
        methodsForMenu.put(10, this::outRemovedMaxSubstring);
        methodsForMenu.put(11, this::outRemovedWithFirstConsonant);
        methodsForMenu.put(12, this::outSortedDescendingByLetter);
        methodsForMenu.put(13, this::outGotPalindrome);
        methodsForMenu.put(14, this::outRemovedEntriesOfFirstLetter);
        methodsForMenu.put(15, this::outReplacedInSentence);
        methodsForMenu.put(16, this::quit);
    }

    public void show() {
        out();
    }

    @Override
    protected void showInfo() {
        LOGGER.info("Menu for TextTask");
        menu.forEach((key, elem) -> LOGGER.info(key + " : " + elem));
    }

    private void outMoreSimilarSentences() {
        LOGGER.info(CONTROLLER.getMoreSimilarSentences());
    }

    private void outByIncreasingWords() {
        CONTROLLER.getByIncreasingWords()
                .forEach(LOGGER::info);
    }

    private void outExceptWord() {
        LOGGER.info(CONTROLLER.getExceptWord());
    }

    private void outQuestionsWords() {
        LOGGER.info("Please, input length of words in questions.");
        if (scan.hasNextInt()) {
            int length = scan.nextInt();
            CONTROLLER.getQuestionsWords(length)
                    .forEach(LOGGER::info);
        } else {
            badInput();
            String logMessage = "Used incorrect length";
            LOGGER.info(logMessage);
            LOGGER.warn(logMessage);
            LOGGER.error(logMessage);
            LOGGER.fatal(logMessage);
        }
    }

    private void outReplacedFirstVowelsWithLargest() {
        CONTROLLER.replaceFirstVowelsWithLargest()
                .forEach(LOGGER::info);
    }

    private void outSortedWithFirstLetter() {
        CONTROLLER.sortWithFirstLetter()
                .forEach(LOGGER::info);
    }

    private void outByPercentsOfVowels() {
        CONTROLLER.getByPercentsOfVowels()
                .forEach(LOGGER::info);
    }

    private void outSortedStartingVowelsWithConsonant() {
        CONTROLLER.sortStartingVowelsWithConsonant()
                .forEach(LOGGER::info);
    }

    private void outSortedIncreasingByLetter() {
        LOGGER.info("Please, input letter that can be included in words for " +
                "sorting.");
        final String letter = scan.next();
        if (checkChar(letter)) {
            CONTROLLER.sortIncreasingByLetter(letter.charAt(0))
                    .forEach(LOGGER::info);
        } else {
            badInput();
        }
    }

    private void printFrequencyOfGivenWords() {
        LOGGER.info("Please, input needed words");
        List<String> allWords = new ArrayList<>();
        scan.nextLine();
        String word = scan.nextLine();
        do {
            allWords.add(word);
            word = scan.nextLine();
        } while (!word.equals(""));
        CONTROLLER.outFrequencyOfGivenWords(allWords)
                .forEach(LOGGER::info);
    }

    private void outRemovedMaxSubstring() {
        LOGGER.info("Please, input start letter.");
        String startLetter = scan.next();
        if (checkChar(startLetter)) {
            LOGGER.info("Please, input end letter.");
            String endLetter = scan.next();
            if (checkChar(endLetter)) {
                CONTROLLER.deleteMaxSubstring(startLetter.charAt(0),
                        endLetter.charAt(0))
                        .forEach(LOGGER::info);
            } else {
                badInput();
            }
        } else {
            badInput();
        }
    }

    private void outRemovedWithFirstConsonant() {
        LOGGER.info("Please, input length of words for deleting.");
        if (scan.hasNextInt()) {
            int length = scan.nextInt();
            CONTROLLER.deleteWithFirstConsonant(length)
                    .forEach(LOGGER::info);
        } else {
            badInput();
        }
    }

    private void outSortedDescendingByLetter() {
        LOGGER.info("Please, input letter that can be included in words for " +
                "sorting.");
        String letter = scan.next();
        if (checkChar(letter)) {
            CONTROLLER.sortDescendingByLetter(letter.charAt(0))
                    .forEach(LOGGER::info);
        } else {
            badInput();
        }
    }

    private void outGotPalindrome() {
        LOGGER.info(CONTROLLER.getPalindrome());
    }

    private void outRemovedEntriesOfFirstLetter() {
        LOGGER.info(CONTROLLER.deleteEntriesOfFirstLetter());
    }

    private void outReplacedInSentence() {
        LOGGER.info("Please, input number of sentence.");
        if (scan.hasNextInt()) {
            int numOfSentence = scan.nextInt();
            LOGGER.info("Please, input length of words for being replaced.");
            if (scan.hasNextInt()) {
                int lengthOfWords = scan.nextInt();
                LOGGER.info("Please, input word that will be used for " +
                        "replacement.");
                String word = scan.next();
                CONTROLLER.replaceInSentence(numOfSentence, lengthOfWords,
                        word)
                        .forEach(LOGGER::info);
            } else {
                badInput();
            }
        } else {
            badInput();
        }
    }

    private boolean checkChar(final String letter) {
        return letter.length() == 1;
    }
}
