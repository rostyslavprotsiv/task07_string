package com.rostyslavprotsiv.model.action;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegExAction {

    public boolean beginCapitalEndPeriod(final String sentence) {
        Pattern p = Pattern.compile("[A-Z].* ");
        Matcher m = p.matcher(sentence);
        return m.matches();
    }

    public String[] splitOnTheGivenWords(final String sentence,
                                         final String... words) {
        String regex = "";
        for (int i = 0; i < words.length - 1; i++) {
            regex += words[i] + "|";
        }
        regex += words[words.length - 1];
        return sentence.split(regex);
    }

    public String replaceVowelsWithUnderscores(final String sentence) {
        return sentence.replaceAll("[aeiou]", "_");
    }
}
