package com.rostyslavprotsiv.model.action;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class LocaleManager {
    private final ResourceManager manager = ResourceManager.INSTANCE;

    public ArrayList<String> getUkrainianMenu() {
        return getMenu("uk", "UA");
    }

    public ArrayList<String> getEnglishMenu() {
        return getMenu("", "");
    }

    public ArrayList<String> getSpainMenu() {
        return getMenu("es", "ES");
    }

    private ArrayList<String> getMenu(final String languageCode,
                                      final String countryCode) {
        ArrayList<String> menu;
        Locale locale = new Locale(languageCode, countryCode);
        manager.changeResource(locale);
        menu = manager.getAll();
        addTime(menu, locale);
        return menu;
    }

    private void addTime(final ArrayList<String> menu, final Locale locale) {
        DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.LONG,
                locale);
        menu.add(menu.size() - 1, dateFormat.format(new Date()));
    }
}
