package com.rostyslavprotsiv.model.action;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Locale;
import java.util.ResourceBundle;

public enum ResourceManager {
    INSTANCE;
    private ResourceBundle resourceBundle;
    private final String resourceName = "menu";

    ResourceManager() {
        resourceBundle = ResourceBundle.getBundle(resourceName,
                Locale.getDefault());
    }

    public void changeResource(final Locale locale) {
        resourceBundle = ResourceBundle.getBundle(resourceName, locale);
    }

    public String getString(final String key) {
        return resourceBundle.getString(key);
    }

    public ArrayList<String> getAll() {
        final Enumeration<String> allMenuEnum = resourceBundle.getKeys();
        ArrayList<String> allMenu = new ArrayList<>();
        String staticPart = "str";
        int counter = 0;
        while (allMenuEnum.hasMoreElements()) {
            counter++;
            allMenuEnum.nextElement();
        }
        for (int i = 1; i < counter; i++) {
            allMenu.add(resourceBundle.getString(staticPart + i));
        }
        allMenu.add(resourceBundle.getString("colorName"));
        return allMenu;
    }

}
