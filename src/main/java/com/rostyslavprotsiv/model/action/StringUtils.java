package com.rostyslavprotsiv.model.action;

import java.util.Optional;
import java.util.stream.Stream;

public class StringUtils<T> {

    @SafeVarargs
    public final String concatenate(final T... objectsForConcatenating) {
        Optional<String> opt = Stream.of(objectsForConcatenating)
                .map(Object::toString)
                .reduce(String::concat);
        return opt.orElse("");
    }
}
