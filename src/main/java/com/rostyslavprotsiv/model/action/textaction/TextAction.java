package com.rostyslavprotsiv.model.action.textaction;

import com.rostyslavprotsiv.model.entity.Text;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * This class was creating for demonstrating work with String(Task 2).
 *
 * @author Rostyslav Protsiv
 * @version 1.0.0 02 May 2019
 * @since 2019-02-05
 */
public class TextAction {
    private final Logger LOGGER = LogManager.getLogger(TextAction.class);
    private final String REG_EX_FOR_PUNCTUATION = "[,.?!]+";
    private final Pattern PATTERN_FOR_PUNCTUATION
            = Pattern.compile(REG_EX_FOR_PUNCTUATION);

    public int findMoreSimilarSentences(final Text text) {
        final List<String> sentences = deletePunctuationMarks(text);
        String[] sentenceFirst;
        int counter;
        int theBiggestCounter = 0;
        for (int i = 0; i < sentences.size(); i++) {
            sentenceFirst = sentences.get(i)
                    .split(" ");
            for (int j = 0; j < sentenceFirst.length; j++) {
                counter = countAnotherSentences(i, j, sentences, sentenceFirst);
                if (counter > theBiggestCounter) {
                    theBiggestCounter = counter;
                }
            }
        }
        return theBiggestCounter;
    }

    public List<String> getAllByIncreasingWords(final Text text) {
        final List<String> sentences = getAllText(text);
        sentences.sort(Comparator.comparingInt(s -> s.split("\\s+").length));
        return sentences;
    }

    public String findSpecificWord(final Text text) {
        final List<String> sentences = deletePunctuationMarks(text);
        final String[] sentenceFirst = sentences.get(0)
                .split(" ");
        String[] sentenceSecond;
        String specificWord = null;
        boolean isSpecific = true;
        for (int i = 0; i < sentenceFirst.length; i++) {
            for (int j = 1; j < sentences.size(); j++) {
                sentenceSecond = sentences.get(j)
                        .split(" ");
                for (int k = 0; k < sentenceSecond.length; k++) {
                    if (sentenceFirst[i].equals(sentenceSecond[k])) {
                        isSpecific = false;
                        break;
                    }
                }
                if (!isSpecific) {
                    break;
                }
            }
            if (isSpecific) {
                specificWord = sentenceFirst[i];
                return specificWord;
            }
            isSpecific = true;
        }
        return specificWord;
    }

    public Set<String> getWordsFromQuestions(final Text text,
                                             final int length) {
        if (length <= 0) {
            throw new IllegalArgumentException("length parameter = "
                    + length + " must be > 0");
        }
        final String REG_EX_FOR_PUNCTUATION = "\\b\\S{" + length + "}\\b";
        final Pattern PATTERN_FOR_PUNCTUATION =
                Pattern.compile(REG_EX_FOR_PUNCTUATION);
        final List<String> allQuestions = getQuestions(text);
        Set<String> words = new HashSet<>();
        allQuestions.forEach(s -> {
            Matcher m = PATTERN_FOR_PUNCTUATION.matcher(s);
            while (m.find()) {
                words.add(m.group());
            }
        });
        return words;
    }

    public List<String> replaceStartedVowelsWithLargest(final Text text) {
        final List<String> sentences = getAllText(text);
        final String regEx = "\\b[аоуеиіАОУЕИІ]\\S*\\b";
        final Pattern p = Pattern.compile(regEx);
        return sentences.stream()
                .map(s -> {
                    Matcher m = p.matcher(s);
                    if (m.find()) {
                        String clearSentence =
                                s.replace(REG_EX_FOR_PUNCTUATION, "");
                        return clearSentence.replace(m.group(),
                                findLargestWord(clearSentence));
                    }
                    return s;
                })
                .collect(Collectors.toList());
    }

    public List<String> getSortedByFirstLetter(final Text text) {
        final List<String> sentences = deletePunctuationMarks(text);
        List<String> changed = new ArrayList<>();
        final List<String> sorted = sentences.stream()
                .map(s -> s.split(" "))
                .flatMap(Arrays::stream)
                .sorted(Comparator.comparingInt(s -> (int) s.charAt(0)))
                .collect(Collectors.toList());
        changed.add(sorted.get(0));
        for (int i = 0; i < sorted.size() - 1; i++) {
            if (sorted.get(i)
                    .charAt(0) != sorted.get(i + 1)
                    .charAt(0)) {
                changed.add("\n   " + sorted.get(i + 1));
            } else {
                changed.add(sorted.get(i + 1));
            }
        }
        return changed;
    }

    public List<String> getByPercentsOfVowels(final Text text) {
        final List<String> sentences = deletePunctuationMarks(text);
        return sentences.stream()
                .map(s -> s.split(" "))
                .flatMap(Arrays::stream)
                .sorted(Comparator.comparingDouble(this::getVowelsPercents))
                .collect(Collectors.toList());
    }

    public List<String> sortStartedVowelsWithConsonant(final Text text) {
        final List<String> sentences = deletePunctuationMarks(text);
        final String regEx = "\\b[аоуеиіАОУЕИІ]\\S*\\b";
        final Pattern p = Pattern.compile(regEx);
        return sentences.stream()
                .map(s -> s.split(" "))
                .flatMap(Arrays::stream)
                .filter(s -> {
                    Matcher m = p.matcher(s);
                    return m.find();
                })
                .sorted(Comparator.comparingInt(this::getFirstConsonantLetter))
                .collect(Collectors.toList());
    }

    public List<String> sortIncreasingByGivenLetter(final Text text,
                                                    final char letter) {
        final List<String> sentences = deletePunctuationMarks(text);
        return sentences.stream()
                .map(s -> s.split(" "))
                .flatMap(Arrays::stream)
                .sorted(Comparator.comparingInt(s -> getCountOfGivenLetter(s,
                        letter)))
                .collect(Collectors.toList());
    }

    public List<String> getFrequencyGivenWords(final Text text,
                                               final List<String> words) {
        final List<String> sentences = deletePunctuationMarks(text);
        Map<String, Long> preparedWords = new TreeMap<>();
        List<Map.Entry<String, Long>> sorted;
        List<String> forOutput = new ArrayList<>();
        for (int i = 0; i < words.size(); i++) {
            if (preparedWords.containsKey(words.get(i))) {
                continue;
            }
            final int k = i;
            long amount = sentences.stream()
                    .map(s -> s.split(" "))
                    .flatMap(Arrays::stream)
                    .filter(s -> words.get(k)
                            .equals(s))
                    .count();
            preparedWords.put(words.get(i), amount);
        }
        sorted = new ArrayList<>(preparedWords.entrySet());
        sorted.sort((a, b) -> (int) (b.getValue() - a.getValue()));
        sorted.forEach(s -> forOutput.add(s.toString()
                .split("=")[0]));
        return forOutput;
    }

    public List<String> deleteSubstring(final Text text, final char start,
                                        final char end) {
        final List<String> sentences = getAllText(text);
        final String regEx = start + ".*" + end;
        final Pattern p = Pattern.compile(regEx, Pattern.DOTALL);
        return sentences.stream()
                .map(s -> {
                    Matcher m = p.matcher(s);
                    if (m.find()) {
                        return s.replaceFirst(s.substring(m.start(), m.end())
                                , "");
                    }
                    return s;
                })
                .collect(Collectors.toList());
    }

    public List<String> deleteStartingOnConsonant(final Text text,
                                                  final int length) {
        final List<String> sentences = getAllText(text);
        final int minLength = 0;
        final int maxLength = 30;
        if (length <= minLength || length > maxLength) {
            throw new IllegalArgumentException("length parameter = "
                    + length + " must be > " + minLength + " or <= " + maxLength);
        }
        final String regEx = "\\b[БбВвГгҐґДдЖжЗзЙйКкЛлМмНнПпРрСсТтФфX" +
                "хЦцЧчШшЩщ]\\S{"
                + (length - 1) + "}" + "\\b";
        return sentences.stream()
                .map(s -> s.replaceAll(regEx, ""))
                .collect(Collectors.toList());
    }

    public List<String> sortDescendingByGivenLetter(final Text text,
                                                    final char letter) {
        final List<String> sentences = deletePunctuationMarks(text);
        return sentences.stream()
                .map(s -> s.split(" "))
                .flatMap(Arrays::stream)
                .sorted(Comparator.comparingInt(s -> -getCountOfGivenLetter(s
                        , letter)))
                .collect(Collectors.toList());
    }

    public String findPalindrome(final Text text) {
        final List<String> sentences = getAllText(text);
        StringBuilder allText = new StringBuilder();
        Pattern p;
        Matcher m;
        for (int i = 0; i < sentences.size() - 1; i++) {
            allText.append(sentences.get(i));
        }
        for (int i = allText.length(); i > 0; i--) {
            p = Pattern.compile(generatePalindromeRegEx(i), Pattern.DOTALL);
            m = p.matcher(allText);
            if (m.find()) {
                return m.group();
            }
        }
        return null;
    }

    public String deleteEntriesFirstLetter(final Text text) {
        final List<String> sentences = getAllText(text);
        final StringBuilder allText = new StringBuilder();
        sentences.stream()
                .map(s -> s.split(" "))
                .flatMap(Arrays::stream)
                .forEach(s -> {
                    String str = s;
                    String firstLetter = s.isEmpty() ? "" :
                            String.valueOf(s.charAt(0));
                    if (s.length() >= 2 && firstLetter.equals("\n")
                            && String.valueOf(s.charAt(1))
                            .equals(String
                                    .valueOf(s.charAt(1))
                                    .toUpperCase())) {
                        firstLetter = String.valueOf(s.charAt(1));
                        str = str.replace(firstLetter.toLowerCase(), "");
                    } else {
                        str = s.replace(firstLetter, "");
                        str = firstLetter + str;
                    }
                    allText.append(str + " ");
                });
        return allText.toString();
    }

    public List<String> replaceGivenSentence(final Text text,
                                             final int sentenceNumber,
                                             final int lengthOfReplaceableWords,
                                             final String subString) {
        final List<String> sentences = getAllText(text);
        final int minLengthOfReplaceableWords = 0;
        final int maxLengthOfReplaceableWords = 40;
        final String regEx = "\\b\\S{" + lengthOfReplaceableWords + "}\\b";
        if (sentenceNumber < 0 || sentenceNumber >= sentences.size()
                || lengthOfReplaceableWords <= minLengthOfReplaceableWords
                || lengthOfReplaceableWords >= maxLengthOfReplaceableWords) {
            String logMessage = "Used incorrect argument ";
            LOGGER.trace(logMessage);
            LOGGER.debug(logMessage);
            LOGGER.info(logMessage);
            LOGGER.warn(logMessage);
            LOGGER.error(logMessage);
            LOGGER.fatal(logMessage);
            throw new IllegalArgumentException();
        }
        sentences.set(sentenceNumber,
                sentences.get(sentenceNumber)
                        .replaceAll(regEx, subString));
        return sentences;
    }

    private String generatePalindromeRegEx(final int size) {
        String regEx = "(.?)";
        final int minSize = 0;
        if (size <= minSize) {
            throw new IllegalArgumentException("size must be > 0");
        }
        for (int i = size; i > 0; i--) {
            regEx = "(.)" + regEx + "\\" + i;
        }
        return regEx;
    }

    private int getCountOfGivenLetter(final String word, final char letter) {
        final String regEx = "[" + letter + "]";
        return getNumberOfEntries(regEx, word);
    }

    private int getFirstConsonantLetter(final String word) {
        final String regEx = "[^аоуеиіАОУЕИІ]";
        final Pattern p = Pattern.compile(regEx);
        final Matcher m = p.matcher(word);
        if (m.find()) {
            return (int) m.group()
                    .charAt(0);
        }
        return Integer.MAX_VALUE;
    }

    private int getNumberOfEntries(final String regEx, final String word) {
        final Pattern p = Pattern.compile(regEx);
        final Matcher m = p.matcher(word);
        int counter = 0;
        while (m.find()) {
            counter++;
        }
        return counter;
    }

    private double getVowelsPercents(final String word) {
        final String regEx = "[аоуеиіАОУЕИІ]";
        return getNumberOfEntries(regEx, word) / (double) word.length();
    }

    private String findLargestWord(final String sentence) {
        final String regEx = "\\b\\S*\\b";
        final Pattern p = Pattern.compile(regEx);
        final Matcher m = p.matcher(sentence);
        int theBiggestLength = 0;
        String theLargestWord = "";
        while (m.find()) {
            if (m.group()
                    .length() > theBiggestLength) {
                theBiggestLength = m.group()
                        .length();
                theLargestWord = m.group();
            }
        }
        return theLargestWord;
    }

    private List<String> getQuestions(final Text text) {
        final Map<Integer, String> allText = text.getReadText();
        List<String> sentences = new ArrayList<>();
        String currentSentence = "";
        Matcher m;
        for (int i = 0; i < allText.size(); i++) {
            m = PATTERN_FOR_PUNCTUATION.matcher(allText.get(i));
            if (m.find()) {
                currentSentence += allText.get(i)
                        .substring(0, m.start()) + " ";
                if (allText.get(i)
                        .charAt(m.end() - 1) == '.'
                        || allText.get(i)
                        .charAt(m.end() - 1) == '!') {
                    currentSentence = "";
                } else if (allText.get(i)
                        .charAt(m.end() - 1) == '?') {
                    currentSentence = currentSentence.trim();
                    sentences.add(currentSentence);
                    currentSentence = "";
                }
            } else {
                currentSentence += allText.get(i) + " ";
            }
        }
        return sentences;
    }

    private int countAnotherSentences(final int sentenceIndex,
                                      final int secondSentenceIndex,
                                      final List<String> sentences,
                                      final String[] sentenceFirst) {
        int counter = 1;
        String[] sentenceSecond;
        for (int k = sentenceIndex + 1; k < sentences.size(); k++) {
            sentenceSecond = sentences.get(k)
                    .split(" ");
            for (int l = 0; l < sentenceSecond.length; l++) {
                if (sentenceFirst[secondSentenceIndex].equals(sentenceSecond[l])) {
                    counter++;
                    break;
                }
            }
        }
        return counter;
    }

    private List<String> deletePunctuationMarks(final Text text) {
        final Map<Integer, String> allText = text.getReadText();
        List<String> sentences = new ArrayList<>();
        String currentSentence = "";
        Matcher m;
        for (int i = 0; i < allText.size(); i++) {
            m = PATTERN_FOR_PUNCTUATION.matcher(allText.get(i));
            if (m.find()) {
                currentSentence += allText.get(i)
                        .substring(0, m.start()) + " ";
                if (allText.get(i)
                        .charAt(m.end() - 1) == '.'
                        || allText.get(i)
                        .charAt(m.end() - 1) == '?'
                        || allText.get(i)
                        .charAt(m.end() - 1) == '!') {
                    currentSentence = currentSentence.trim();
                    sentences.add(currentSentence);
                    currentSentence = "";
                }
            } else {
                currentSentence += allText.get(i) + " ";
            }
        }
        return sentences;
    }

    private List<String> getAllText(final Text text) {
        List<String> sentences = new ArrayList<>();
        final Map<Integer, String> allText = text.out();
        StringBuilder sentence = new StringBuilder();
        for (int i = 0; i < allText.size(); i++) {
            if (allText.get(i)
                    .charAt(allText.get(i)
                            .length() - 2) == '.'
                    || allText.get(i)
                    .charAt(allText.get(i)
                            .length() - 2) == '?'
                    || allText.get(i)
                    .charAt(allText.get(i)
                            .length() - 2) == '!') {
                sentence.append(allText.get(i));
                sentences.add(sentence.toString());
                sentence = new StringBuilder();
                continue;
            }
            sentence.append(allText.get(i));
        }
        return sentences;
    }
}
