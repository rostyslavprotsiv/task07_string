package com.rostyslavprotsiv.model.creator;

import com.rostyslavprotsiv.model.entity.TestObject;

import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TestObjectCreator {

    public TestObject[] creator() {
        final Random random = new Random();
        final int maxSizeOfMass = 5;
        final int maxSizeOfName = 7;
        final int maxId = 30;
        final int minChar = 65;
        final int maxChar = 122;
        return Stream
                .generate(() -> new TestObject(Stream
                        .generate(() -> Stream
                                .iterate("",
                                        s -> String.valueOf((char) (random.nextInt(maxChar - minChar) + minChar)))
                                .limit(random.nextInt(maxSizeOfName) + 2)
                                .collect(Collectors.joining()))
                        .limit(1)
                        .collect(Collectors.joining()), random.nextInt(maxId)))
                .limit(maxSizeOfMass)
                .toArray(TestObject[]::new);
    }
}
