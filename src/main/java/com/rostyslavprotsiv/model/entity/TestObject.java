package com.rostyslavprotsiv.model.entity;

import java.util.Objects;

public class TestObject {
    private String name;
    private int id;

    public TestObject(final String name, final int id) {
        this.name = name;
        this.id = id;
    }

    public TestObject() {
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TestObject that = (TestObject) o;
        return id == that.id &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, id);
    }

    @Override
    public String toString() {
        return "TestObject{" +
                "name='" + name + '\'' +
                ", id=" + id +
                '}';
    }
}
