package com.rostyslavprotsiv.model.entity;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class Text {
    private final HashMap<Integer, String> READ_TEXT = new HashMap<>();
    private final String PATH;
    private final String ENCODING = "Cp1251";

    public Text(final String PATH) {
        this.PATH = PATH;
        try {
            read();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Map<Integer, String> out() {
        Map<Integer, String> allText = new HashMap<>(READ_TEXT);
        return allText.entrySet()
                .stream()
                .collect(Collectors.toMap(Map.Entry::getKey,
                        s -> {
                            s.setValue(s.getValue() + " ");
                            if (String.valueOf(s.getValue()
                                    .charAt(0))
                                    .equals(String.valueOf(s.getValue()
                                            .charAt(0))
                                            .toUpperCase())) {
                                s.setValue("\n" + s.getValue());
                            }
                            return s.getValue();
                        }));
    }

    public HashMap<Integer, String> getReadText() {
        return READ_TEXT;
    }

    private void read() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(
                new FileInputStream(PATH), ENCODING));
        String tmp;
        String[] sentence;
        int previousLength = 0;
        while ((tmp = br.readLine()) != null) {
            sentence = tmp.split("\\s+");
            for (int i = previousLength; i < sentence.length + previousLength; i++) {
                READ_TEXT.put(i, sentence[i - previousLength]);
            }
            previousLength = sentence.length + previousLength;
        }
    }

    @Override
    public String toString() {
        return "Text{" +
                "READ_TEXT=" + READ_TEXT +
                ", PATH='" + PATH + '\'' +
                ", ENCODING='" + ENCODING + '\'' +
                '}';
    }
}